Git Commands

mkdir - make directory/folder
cd - change directory
pwd - print working directory
ls - list
touch - creating files


======
SSH Key
======

1. Open a terminal
   open the 'windows terminal'
2. Create an SSH Key
   Terminal
     ssh-keygen
3. Copy the SSH Key
4. Configure the git account in the device/project
   configure the global user email
   git config --global user.email "[]"
   git config --global user.name "[]"

=======================
Git Commands
=======================
1. Initialize a local git repository
   git init
2. Peek at the states of the files/folders
   git status
3. Stage the file in preparation for creating a commit
   staging files  individually
     git add [filename]
   staging all files
     git add .
     git add -A
4. Peek at  the states of the files/folders
   git status
5. Create Commit
   git commit -m "[message]"